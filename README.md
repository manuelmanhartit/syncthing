# Syncthing 1.2

This runs a Syncthing container - I use it for synchronizing my documents, fotos, music, tutorials, etc. over various devices.

You have to create an `.env` file for the configuration of the container to fit your local machine. Please make sure that you backup the syncthing configuration yourself somehow.

If you have more generic questions please have a look [here](https://cms.manhart.space/dockerisierung)

## Getting Started

Copy the `.env-example` file into a `.env` file and edit the variables to fit your environment. That means, if you dont share the same thing I do (which is very likely), you have to remove / add the variables in your `.env` file.

    # cp .env-example .env
    # vi .env

After you edited your `.env` file, you have to map the volume mounts accordingly

    # vi docker-compose.yml

Start the container with the command:

    # sudo docker-compose up -d

Your Syncthing service is available on 127.0.0.1:8384

### Manual steps

On the first time you have to add the computers to share data with and the folders which you want to sync.

### Build / Extend

-

### Replicating the service

I suggest that you start with [Getting started](#markdown-header-getting-started) like it is a new service.

### Upgrading the service / containers

For upgrading the container, you will need to call

	# docker-compose down
	
to tear down the service.

Then pull the latest version with

	# docker-compose pull

Lastly start the service with

	# docker-compose up -d

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/syncthing/downloads/?tab=tags).

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author.

### Changelog

__1.2__

* Updated readme
* Changed license
* Put into public bitbucket repo

### Open issues

* Uses ROOT as user - which is already defined in the syncthing/syncthing image.
* Uses the host network

## Sources

* [Docker](http://www.docker.io/) - The container base
* [Docker-Compose](https://docs.docker.com/compose/) - Composing multiple containers into one service
* [VS Code](https://code.visualstudio.com/) - Used to edit all the files
* [Syncthing Image](https://hub.docker.com/r/syncthing/syncthing)
* [Adding a Healthcheck](https://howchoo.com/g/zwjhogrkywe/how-to-add-a-health-check-to-your-docker-container)
